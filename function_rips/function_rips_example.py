#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon May 17 12:15:29 2021

@author: arolle
"""

import numpy as np
from sklearn.metrics.pairwise import euclidean_distances
from sklearn.neighbors import KernelDensity
import matplotlib.pyplot as plt
import time
from scipy.spatial import KDTree

from function_rips import function_rips
from generating_pointclouds import draw_points_from_sphere
from generating_pointclouds import draw_points_from_annulus

##########################################################
# Draw samples from a sphere, and compute the scc2020 file
##########################################################

pointcloud = draw_points_from_sphere(num_points=1000, 
                                     sample_weight=0.9, 
                                     r=0.75, north_pole_weight=0.3)


#fig = plt.figure()
#ax = fig.add_subplot(projection='3d')
#plt.xlim([-2, 2])
#plt.ylim([-2, 2])
#ax.set_zlim([-2, 2])
    
#xs = pointcloud[:, 0]
#ys = pointcloud[:, 1]
#zs = pointcloud[:, 2]

#ax.scatter(xs, ys, zs)

#ax.set_xlabel('x')
#ax.set_ylabel('y')
#ax.set_zlabel('z')

#plt.show()

file_name = 'function_rips_sphere.txt'

band = 0.2

kde = KernelDensity(kernel='gaussian', bandwidth=band).fit(pointcloud)
function_vals = np.exp(kde.score_samples(pointcloud))

start = time.time()

dm = euclidean_distances(pointcloud)

function_rips(dm=dm, function_vals=function_vals, d=3, 
                  file_name=file_name, 
                  give_radius=False, r=0.1, 
                  give_num_simplices=True,
                  num_simplices=10000)

end = time.time()
runtime = end - start
print('On the sphere pointcloud, function rips took ' + str(runtime))


############################################################
# Draw samples from an annulus, and compute the scc2020 file
############################################################


pointcloud = draw_points_from_annulus(num_points=1000, sample_weight=0.9, 
                                      r=1.0, R=2.0, 
                                      num_gaussians=20, var=0.1)

data_to_plot = pointcloud

figsize = (10,10)
dotsize = 10
plt.figure(figsize=figsize)
plt.xlim(-3, 3)
plt.ylim(-3, 3)
plt.scatter(data_to_plot.T[0], data_to_plot.T[1], s = dotsize)
plt.grid()

plt.show()

file_name = 'function_rips_annulus.txt'

band = 0.1

kde = KernelDensity(kernel='gaussian', bandwidth=band).fit(pointcloud)
function_vals = np.exp(kde.score_samples(pointcloud))

dm = euclidean_distances(pointcloud)

start = time.time()

function_rips(dm=dm, function_vals=function_vals, d=2, 
                  file_name=file_name, 
                  give_radius=False, r=0.2, 
                  give_num_simplices=True,
                  num_simplices=10000)

end = time.time()
runtime = end - start
print('On the annulus pointcloud, function rips took ' + str(runtime))
