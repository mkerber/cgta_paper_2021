#!/bin/bash
date
for points in 50000 100000 200000 400000 800000
do
let simplices=$points*20
echo $simplices
for ((i=1; i<=5; i++))
  do
    date
    echo $simplices simplices, run $i
    python3 create_sphere_instance.py $points $simplices function_rips_with_threshold_${simplices}_$i.scc
  done
done
date
