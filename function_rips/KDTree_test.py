#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed May 19 20:05:15 2021

@author: arolle
"""

import time
from scipy.spatial import cKDTree
from generating_pointclouds import draw_points_from_sphere

pointcloud = draw_points_from_sphere(num_points=10000, 
                                     sample_weight=1.0, 
                                     r=0.75, north_pole_weight=0)

start = time.time()

print("Build KD-tree")

kd_tree = cKDTree(pointcloud)

num_pairs = 0
r = 0.01

print("Loop")

while num_pairs < 3000000:
    pairs = kd_tree.query_pairs(r=r)
    num_pairs = len(pairs)
    print("r, num_pairs",r,num_pairs)
    print("count neighbors", kd_tree.count_neighbors(kd_tree,r=r))
    r = 2 * r

end = time.time()
runtime = end - start
print('kd tree took ' + str(runtime))

print(len(pairs))
