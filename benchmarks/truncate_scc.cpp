#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <vector>

int main(int argc, char** argv) {
  if(argc<2) {
    std::cerr << "Input file?" << std::endl;
    std::exit(1);
  }
  std::string infile(argv[1]);
  std::ifstream ifstr(infile);
  
  int dim;

  if(argc>=3) {
    dim = std::atoi(argv[2]);
  } else {
    dim=1;
  }
  
  std::cout << "scc2020\n2\n";
  
  std::string dummy;

  std::getline(ifstr,dummy);
  std::getline(ifstr,dummy);
  std::getline(ifstr,dummy);

  std::stringstream sstr(dummy);
  
  std::vector<long> dims;

  while(sstr.good()) {
    long next;
    sstr >> next;
    //std::cerr << "Next: " << next << std::endl;
    dims.push_back(next);
  }

  int d = dims.size();

  std::cout << dims[d-dim-3] << " " << dims[d-dim-2] << " " << dims[d-dim-1] << std::endl;
  for(int i=0;i<d-dim-3;i++) {
    for(int j=0;j<dims[i];j++) {
      std::getline(ifstr,dummy);
    }
  }
  for(int i=d-dim-3;i<=d-dim-2;i++) {
    for(int j=0;j<dims[i];j++) {
      std::getline(ifstr,dummy);
      std::cout << dummy << std::endl;
    }
  }
    
  ifstr.close();
  
  return 0;
}
