for file in large_datasets/*.scc
do
  date
  echo $file
  wc -l $file
  ./multi_chunk_instance.sh $file $file.compressed
  input=$(awk 'BEGIN {print ARGV[1]}' $(du -sb $file))
  echo Input file size: $input
  output=$(awk 'BEGIN {print ARGV[1]}' $(du -sb $file.compressed))
  echo Output file size: $output
  echo File compression rate: $( awk 'BEGIN {print ARGV[1]/ARGV[2]*100}' $output $input) 

done

for file in large_datasets/*.scc.trunc
do
  date
  echo $file
  ./multi_chunk_instance.sh $file $file.compressed
  echo Input file size: $input
  output=$(awk 'BEGIN {print ARGV[1]}' $(du -sb $file.compressed))
  echo Output file size: $output
  echo File compression rate: $( awk 'BEGIN {print ARGV[1]/ARGV[2]*100}' $output $input) 
done
echo Finished
date
