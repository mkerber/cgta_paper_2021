for file in large_datasets/*.scc
do
  date
  echo $file
  rm -f bla bla_*
  echo No multi-chunk, sequential
  /usr/bin/time --format "RESULTS %C\nTime: %e\nMemory: %M" timeout 3600 ~/cgta_paper_2021/benchmarks/full_presentation_sequential $file bla
  echo No multi-chunk, parallel
  /usr/bin/time --format "RESULTS %C\nTime: %e\nMemory: %M" timeout 3600 ~/cgta_paper_2021/benchmarks/full_presentation $file bla
  echo Multi-chunk, sequential
  /usr/bin/time --format "RESULTS %C\nTime: %e\nMemory: %M" timeout 3600 ~/cgta_paper_2021/benchmarks/full_presentation_sequential --multi-chunk $file bla
  echo Multi-chunk, parallel
  /usr/bin/time --format "RESULTS %C\nTime: %e\nMemory: %M" timeout 3600 ~/cgta_paper_2021/benchmarks/full_presentation --multi-chunk $file bla
  for blafile in bla_*
  do
    cat $blafile >> bla
  done
  input=$(awk 'BEGIN {print ARGV[1]}' $(du -sb $file))
  echo Input file size: $input
  output=$(awk 'BEGIN {print ARGV[1]}' $(du -sb bla))
  echo Output file size: $output
  echo File compression rate: $( awk 'BEGIN {print ARGV[1]/ARGV[2]*100}' $output $input) 
done

echo Finished
date
