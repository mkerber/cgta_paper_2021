for file in comparison_with_rivet_datasets/*.firep
do
  date
  echo $file
  wc -l $file
  ./rivet_instance.sh $file dummy1
  ./mpfree_instance.sh $file dummy2
  ./rivet_instance.sh $file.snapped_50_50 dummy3 
  ./mpfree_instance.sh $file.snapped_50_50 dummy4 
done
echo "Finished"
date