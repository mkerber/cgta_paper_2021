#include <mpfree/mpfree.h>
#include <multi_chunk/multi_chunk.h>

#include<iomanip>

#if MPFREE_TIMERS
boost::timer::cpu_timer copy_timer,total_timer;
#endif

template<typename GrMat> void matrices_equal(GrMat& M1, GrMat& M2) {
  if(M1.get_num_cols()!=M2.get_num_cols()) {
    std::cout << "DIFFER in no columns";
  }
  if(M1.num_rows!=M2.num_rows) {
    std::cout << "DIFFER in no rows";
  }
  for(int i = 0; i < M1.get_num_cols();i++) {
    std::vector<mpfree::index> col1,col2;
    M1.get_col(i,col1);
    M2.get_col(i,col2);
    if(col1.size()!=col2.size()) {
      std::cout << "DIFFER in size of column " << i << std::endl;
    }
    std::cout << "Column " << i << std::endl;
    for(int j=0;j<col1.size();j++) {
      std::cout << col1[j] << " vs " << col2[j] <<std::endl;
      if(col1[j]!=col2[j]) {
	std::cout << "DIFFER in column " << i << std::endl;
      }
    }
  }
}

int main(int argc, char** argv) {

  std::string infile="";
  std::string outprefix="bla";
  bool use_multi_chunk=false;

  for(int i=1;i<argc;i++) {
    if(std::string(argv[i])=="--multi-chunk") {
      std::cout << "With multi-chunk" << std::endl;
      use_multi_chunk=true;
    } else {
      if(infile=="") {
	infile=argv[i];
      } else {
	outprefix=argv[i];
      }
    }
    
  }

#if MPFREE_TIMERS
  mpfree::initialize_timers();
  total_timer.start();
  copy_timer.start();
  copy_timer.stop();
#endif

#if MPFREE_TIMERS
  mpfree::io_timer.resume();
#endif
  
  typedef multi_chunk::Graded_matrix<phat::vector_vector> Multi_chunk_GrMat;

  std::vector<Multi_chunk_GrMat> matrices;
  
  std::ifstream ifstr(infile);
  typedef typename Multi_chunk_GrMat::Coordinate_traits Grade_cast;
  scc::Scc<Grade_cast> parser(ifstr,&Grade_cast::get_instance());
  mpp_utils::verbose=true;
  mpp_utils::create_graded_matrices_from_scc2020(parser,matrices);
  


 

#if MPFREE_TIMERS
  mpfree::io_timer.stop();
#endif

  std::cout << "matrices.size()==" << matrices.size() << std::endl;

  multi_chunk::verbose=true;

#if MULTI_CHUNK_TIMERS
  multi_chunk::initialize_timers();
#endif


  if(use_multi_chunk) {
    multi_chunk::compress(matrices);


  }



  mpfree::verbose=true;

  for(int i=0;i<=matrices.size()-2;i++) {

#if MPFREE_TIMERS
  copy_timer.resume();
#endif   
  typedef mpfree::Graded_matrix<phat::vector_vector> Mpfree_GrMat;
  Mpfree_GrMat M1(matrices[i]);
  Mpfree_GrMat M2(matrices[i+1]);
#if MPFREE_TIMERS
  copy_timer.stop();
#endif    

    std::string outfile=outprefix+"_"+std::to_string(i);
    mpfree::compute_minimal_presentation(M1,
					 M2,
					 outfile,
					 false);
  }

  double overall_time = total_timer.elapsed().wall;


  std::cout.width(22);
  std::cout << std::fixed << std::setprecision(6);

  

  std::cout << "Overall time:          " << double(total_timer.elapsed().wall)/std::pow(10,9) << std::endl;

  if(use_multi_chunk) {
#if MULTI_CHUNK_TIMERS
    multi_chunk::print_timers(overall_time,false);
#endif
  }

#if MPFREE_TIMERS
  mpfree::print_timers(overall_time,false);
  std::cout << "Copy timer:            " << double(copy_timer.elapsed().wall)/std::pow(10,9) <<  "     ( " << double(copy_timer.elapsed().wall)/overall_time*100 << "% )"<< std::endl;
#endif

  return 0;
}
