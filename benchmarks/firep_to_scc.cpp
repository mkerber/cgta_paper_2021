#include <iostream>
#include <fstream>
#include <string>

int main(int argc, char** argv) {
  if(argc<3) {
    std::cerr << "Input and output file?" << std::endl;
    std::exit(1);
  }
  std::string infile(argv[1]);
  std::string outfile(argv[2]);

  std::ifstream ifstr(infile);
  std::ofstream ofstr(outfile);
  
  ofstr << "scc2020\n2\n";
  
  std::string dummy;

  std::getline(ifstr,dummy);
  std::getline(ifstr,dummy);
  std::getline(ifstr,dummy);
  
  long x,y,z;
  ifstr >> x >> y >> z;
  std::getline(ifstr,dummy);
  ofstr << x << " " << y << " " << z << std::endl;
  long n = x+y;
  for(int i=0;i<n;i++) {
    std::getline(ifstr,dummy);
    ofstr << dummy << std::endl;
  }

  ifstr.close();
  ofstr.close();

  return 0;
}
