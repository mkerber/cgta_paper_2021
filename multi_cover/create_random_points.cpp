#include <CGAL/Random.h>
#include <iostream>
#include <cstdlib>
#include <cmath>

int main(int argc, char** argv) {

#if 1

  if(argc<2) {
    std::cout << "Arguments: #points dim[=2]" << std::endl;
  }

  int n = atoi(argv[1]);
  
  int d = 2;
  
  if (argc>=3) {
    d = atoi(argv[2]);
  }

  CGAL::Random rand;
  
  for(int i=0;i<n;i++) {
    for(int j=0;j<d;j++) {
      std::cout << rand.get_double() << " ";
    }
    std::cout << std::endl;
  }

#else
  
  CGAL::Random rand;

  for(int i=0;i<100;i++) {
    double rad = (double)i *2*M_PI/100;
    double x = 2*cos(rad)+0.001*rand.get_double();
    double y = 2*sin(rad)+0.001*rand.get_double();
    double z = 0.001*rand.get_double();
    std::cout << x << " " << y << " " << z << std::endl;
  }

#endif


  return 0;
}
