#!/bin/bash
for ((i=1; i<=$3; i++))
  do
    date
    ./create_random_points $1 3 > temp_instance.txt
    ~/computing-k-fold-cover-persistence/georgs_code/main temp_instance.txt  multi_cover_$1_$2_$i.txt 3 $2 scc2020 > /dev/null
    wc -l multi_cover_$1_$2_$i.txt
    sleep 1
  done
date